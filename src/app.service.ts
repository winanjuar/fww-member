import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { RegisterRequestDto } from './dto/request/register.request.dto';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
} from 'amazon-cognito-identity-js';
import { IRegisterUser } from './interface/register-user.interface';
import { LoginRequestDto } from './dto/request/login.request.dto';
import { ITokenLogin } from './interface/token-login.interface';
import { IAWSError } from './interface/aws-error.interface';
import { AuthConfig } from './auth/auth.config';
import { MemberRepository } from './repository/member.repository';
import { IStatusMember } from './interface/status-member.interface';
import { UpdateProfileRequestDto } from './dto/request/update-profile.request.dto';
import { Member } from './entity/member.entity';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  private readonly userPool: CognitoUserPool;

  constructor(
    private authConfig: AuthConfig,
    private readonly memberRepo: MemberRepository,
  ) {
    this.userPool = new CognitoUserPool({
      UserPoolId: this.authConfig.userPoolId,
      ClientId: this.authConfig.clientId,
    });
  }

  async register(registerDto: RegisterRequestDto): Promise<IRegisterUser> {
    const { username, email, phone, password } = registerDto;

    const newUser = new Promise<IRegisterUser>((resolve, reject) => {
      return this.userPool.signUp(
        username,
        password,
        [
          new CognitoUserAttribute({ Name: 'email', Value: email }),
          new CognitoUserAttribute({
            Name: 'phone_number',
            Value: phone,
          }),
        ],
        null,
        (error, result) => {
          if (!result) {
            const listExceptions = [
              'UsernameExistsException',
              'InvalidPasswordException',
            ];
            if (listExceptions.includes(error.name)) {
              reject({
                name: error.name,
                response: {
                  statusCode: 422,
                  message: error.message,
                  error: 'Unprocessable Entity',
                },
              } as IAWSError);
            } else {
              reject({
                name: error.name,
                response: {
                  statusCode: 500,
                  message: error.message,
                  error: 'Internal Serval Error',
                },
              } as IAWSError);
            }
          } else {
            const user: IRegisterUser = {
              cognitoId: result.userSub,
              username,
              email,
              phone,
            };
            this.logger.log('Register new member to cognito');
            resolve(user);
          }
        },
      );
    });

    const dataMember = await newUser;
    const newMember = await this.memberRepo.saveMember(dataMember);
    this.logger.log('Process data new member');
    return newMember;
  }

  async login(user: LoginRequestDto): Promise<ITokenLogin> {
    const { username, password } = user;
    const authenticationDetails = new AuthenticationDetails({
      Username: username,
      Password: password,
    });

    const userData = {
      Username: username,
      Pool: this.userPool,
    };
    const cognitoUser = new CognitoUser(userData);

    const token = new Promise<ITokenLogin>((resolve, reject) => {
      return cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          const accessToken = result.getIdToken().getJwtToken();
          const refreshToken = result.getRefreshToken().getToken();
          this.logger.log('Login cognito successfully');
          resolve({ accessToken, refreshToken } as ITokenLogin);
        },
        onFailure: (error) => {
          const listExceptions = [
            'NotAuthorizedException',
            'UserNotConfirmedException',
          ];
          if (listExceptions.includes(error.code)) {
            reject({
              name: error.code,
              response: {
                statusCode: 401,
                message: error.message,
                error: 'Unauthorize',
              },
            } as IAWSError);
          } else {
            reject({
              name: error.code,
              response: {
                statusCode: 500,
                message: error.message,
                error: 'Internal Server Error',
              },
            } as IAWSError);
          }
        },
      });
    });

    this.logger.log('Login successfully');
    return await token;
  }

  async checkStatus(id: number): Promise<IStatusMember> {
    const member = await this.memberRepo.findMemberById(id);
    if (!member) {
      throw new NotFoundException('Member not found');
    }

    this.logger.log('Check status member successfully');
    return {
      id,
      isBlacklist: member.status === 'BLACKLIST' ? true : false,
    } as IStatusMember;
  }

  async updateMember(
    cognitoId: string,
    memberDto: UpdateProfileRequestDto,
  ): Promise<Member> {
    const member = await this.memberRepo.updateMember(cognitoId, memberDto);
    if (!member) {
      throw new NotFoundException('Member not found');
    }
    this.logger.log('Process data member');
    return member;
  }
}
