import { OmitType } from '@nestjs/swagger';
import { MemberDto } from './member.dto';

export class MemberExludePasswordDto extends OmitType(MemberDto, [
  'password',
]) {}
