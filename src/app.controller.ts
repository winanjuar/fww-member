import {
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  Post,
  Put,
  UnauthorizedException,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';

import { AppService } from './app.service';
import { RegisterRequestDto } from './dto/request/register.request.dto';
import { LoginRequestDto } from './dto/request/login.request.dto';
import { CognitoUserDto } from './dto/base/cognito-user.dto';
import { GetUser } from './decorator/get-user.decorator';
import JwtGuard from './auth/jwt.guard';
import { ApiBasicAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UpdateProfileRequestDto } from './dto/request/update-profile.request.dto';

@Controller({ version: '1' })
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly appService: AppService) {}

  @Post('register')
  async register(@Body() registerDto: RegisterRequestDto) {
    this.logger.log('[POST] /api/v1/register');
    try {
      const newUser = await this.appService.register(registerDto);
      this.logger.log('Return new user');
      return newUser;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @HttpCode(200)
  @Post('login')
  async login(@Body() loginRequest: LoginRequestDto) {
    this.logger.log('[POST] /api/v1/login');
    try {
      const token = await this.appService.login(loginRequest);
      this.logger.log('Return token');
      return token;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 401) {
        throw new UnauthorizedException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(JwtGuard)
  @Get('whoami')
  getMe(@GetUser() user: CognitoUserDto) {
    this.logger.log('[GET] /api/v1/whoami');
    this.logger.log('Return user logged in');
    return user;
  }

  @UseGuards(JwtGuard)
  @Put('update')
  async updateProfile(
    @GetUser() user: CognitoUserDto,
    @Body() memberDto: UpdateProfileRequestDto,
  ) {
    this.logger.log(`[PUT] /api/v1/update/${user.cognitoId}`);
    try {
      const member = await this.appService.updateMember(
        user.cognitoId,
        memberDto,
      );
      this.logger.log('Return updated member');
      return member;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 401) {
        throw new UnauthorizedException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @ApiBasicAuth()
  @UseGuards(AuthGuard('basic'))
  @Get('is-blacklist/:id')
  async isBlacklist(@Param('id') id: number) {
    this.logger.log(`[GET] /api/v1/is-blacklist/${id}`);
    try {
      const member = await this.appService.checkStatus(id);
      this.logger.log('Return status member');
      return member;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 401) {
        throw new UnauthorizedException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }
}
