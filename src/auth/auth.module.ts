import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthBasicStrategy } from './auth-basic.strategy';
import { ConfigModule } from '@nestjs/config';
import { AuthConfig } from './auth.config';
import { CognitoStrategy } from './cognito.strategy';

@Module({
  imports: [PassportModule, ConfigModule],
  providers: [AuthBasicStrategy, AuthConfig, CognitoStrategy],
  exports: [AuthConfig],
})
export class AuthModule {}
