export interface IRegisterUser {
  cognitoId: string;
  username: string;
  email: string;
  phone: string;
}
