import { Injectable, Logger } from '@nestjs/common';
import { UpdateProfileRequestDto } from 'src/dto/request/update-profile.request.dto';
import { Member } from 'src/entity/member.entity';
import { IRegisterUser } from 'src/interface/register-user.interface';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class MemberRepository extends Repository<Member> {
  private readonly logger = new Logger(MemberRepository.name);
  constructor(dataSource: DataSource) {
    super(Member, dataSource.createEntityManager());
  }

  async saveMember(member: IRegisterUser): Promise<Member> {
    const newMember = await this.save(member);
    this.logger.log('Insert new member');
    return newMember;
  }

  async updateMember(
    cognitoId: string,
    dataMember: UpdateProfileRequestDto,
  ): Promise<Member> {
    let member = await this.findMemberByCognitoId(cognitoId);
    member = { ...member, ...dataMember };
    await this.save(member);

    this.logger.log('Update data member');
    return member;
  }

  async findMemberById(id: number): Promise<Member> {
    const member = await this.findOneBy({ id });
    this.logger.log('Query data member by id');
    return member;
  }

  async findMemberByCognitoId(cognitoId: string): Promise<Member> {
    const member = await this.findOne({ where: { cognitoId } });
    this.logger.log('Query data member by cognitoId');
    return member;
  }
}
